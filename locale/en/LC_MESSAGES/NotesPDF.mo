��          �   %   �      p     q  (   �     �     �     �     �     �                 B   %  ~   h  #   �               8     W     k     x     �     �  G   �  ?   �     9     ?  	   K     U  M  [     �     �     �     �     �            	   &     0     8  ?   ?  k     $   �                @     W     f     r     �     �  ;   �  ?   �     	     	     (	     /	                                                                                
                                       	    %d de %B del %Y A continuación personalice los apuntes. A plugin to export notes in PDF Apuntes Apuntes  Apuntes en PDF Apuntes para el grupo  Automáticos Autores BUTTONAceptar Error al generar los apuntes. Inténtelo de nuevo en unos minutos. Este documento es únicamente una recopilación de ideas. En ningún caso sustituye al material proporcionado por el profesor. Genera apuntes de este grupo en PDF Generar Apuntes Generar Apuntes Automáticos Generar Apuntes Personalizados Ideas Seleccionadas Login first! Not logged in. Personalización de Apuntes Puntuación:  Se seleccionarán los tweets con la máxima puntuación hasta la fecha. There was a problem with your session token. Try again, please. Todos Top tags:   Usuario:  error Project-Id-Version: 
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2017-07-11 17:03+0200
PO-Revision-Date: 2017-07-11 17:05+0200
Last-Translator: 
Language-Team: 
Language: en
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.7.1
Plural-Forms: nplurals=2; plural=(n != 1);
 %d of %B of %Y Now, customize the notes A plugin to export notes in PDF Notes Lecture Notes Notes in PDF Notes for the group Automatic Authors Accept Error while generating the notes. Try it again in a few minutes This document is just an aggregation of ideas. In no way it replaces the material provided by the professor Generates notes of this group in PDF Generate notes Generate Automatic Lecture Notes Generate Lecture Notes Selected ideas Login First Not logged in Notes customization Score:  Tweets with the maximum score up to today will be selected. There was a problem with your session token. Try again, please. All Top tags:   User:  error 