<?php

/**
 * 
 *  Bolotweet-Notes
    Copyright (C) 2018  bolotweet

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author   Alvaro Ortego <alvorteg@ucm.es>
 *
 */
define('STATUSNET', true);
define('LACONICA', true); // compatibility
define('INSTALLDIR', realpath(dirname(__FILE__) . '/../../../..'));
require_once(INSTALLDIR. '/lib/language.php');
require_once(INSTALLDIR . '/lib/common.php');
require_once INSTALLDIR . '/local/plugins/NotesPDF/classes/NotesPDF.php';
$name="NotesPDF";
$path = common_config('plugins', 'locale_path');
 $path = common_config('plugins', 'locale_path');
            if (!$path) {
                $path = INSTALLDIR . "/plugins/$name/locale/en";

if (!file_exists($path)) {                     $path = INSTALLDIR . "/local/plugins/$name/locale";
            }
            }
            if (file_exists($path) && is_dir($path)) {
                bindtextdomain($name, $path);
                bind_textdomain_codeset($name, 'UTF-8');
            }

$tag = ($_POST['tag'] == 'Todos') ? '%' : $_POST['tag'];
$grade = ($_POST['grade'] == 'Todos') ? '%' : $_POST['grade'];
$groupid = $_POST['groupid'];

$users = NotesPDF::getUsersinGroupWithHashtagAndGrade($groupid, $tag, $grade);
//common_log(LOG_ERR, "locale " .locale_get_default()." todos "._m("All"). _m('Generar Apuntes Automáticos'));
echo '<option value="All">'._m("All").'</option>';

for ($i = 0; $i < count($users); $i++) {
    echo '<option value="' . $users[$i] . '">' . $users[$i] . '</option>';
}
    
    
    
