<?php

/**
 * 
 *  Bolotweet-Notes
    Copyright (C) 2018  bolotweet

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author   Alvaro Ortego <alvorteg@ucm.es>
 *
 */
if (!defined('STATUSNET') && !defined('LACONICA')) {
    exit(1);
}

class Notescustomizeform extends Form {

    protected $idGroup = null;

    function __construct($out = null, $id) {
        parent::__construct($out);

        $this->idGroup = $id;
    }

    function id() {

        return 'notes-customize' . $this->idGroup;
    }

    function action() {

        return common_local_url('notesgenerate');
    }

    function sessionToken() {
        $this->out->hidden('token-' . $this->idGroup, common_session_token());
    }

    function formClass() {
        return 'form_notes_customize';
    }

    function formData() {
	$user = common_current_user();
        $this->out->hidden('group-h-' . $this->idGroup, $this->idGroup, 'idGroup');


        // Box para apuntes automáticos
        $this->out->elementStart('div', array('class' => 'notes-div-auto'));
        $this->out->element('p', 'notes-text-auto', _m('Generar Apuntes Automáticos'));
        $this->out->element('p', null, _m('Se seleccionarán los tweets con la máxima puntuación hasta la fecha.'));
        $this->out->elementStart('div');
        $this->out->submit('notes-submit-auto', _m('BUTTON', 'Aceptar'), 'submit', 'submit-auto');
        $this->out->elementEnd('div');
        $this->out->elementEnd('div');


        // Box para apuntes personalizados
        $this->out->elementStart('div', array('class' => 'notes-div-manual'));
        $this->out->element('p', 'notes-text-manual', _m('Generar Apuntes Personalizados'));

        $this->out->elementStart('div');
        $this->out->element('p', 'notes-manual-option', 'Hashtag: ');
        $this->out->elementStart('select', array('name' => 'combo-tag', 'id' => 'notes-combo-hashtag', 'class' => 'notes-combo-manual', 'onchange' => 'changeComboTag(' . $this->idGroup . ');'));

        $tags = NotesPDF::getTagsOfUserWithGradeInGroup($this->idGroup, '%', '%');

        $this->out->element('option', array('value' => 'Todos'), _m('Todos'));

        for ($i = 0; $i < count($tags); $i++) {
            $this->out->element('option', array('value' => $tags[$i]), $tags[$i]);
        }

        $this->out->elementEnd('select');
        $this->out->elementEnd('div');
	if ($user->hasRole("grader")){
		$this->out->elementStart('div');
		$this->out->element('p', 'notes-manual-option', _m('Usuario: '));
		$this->out->elementStart('select', array('name' => 'combo-user', 'id' => 'notes-combo-user', 'class' => 'notes-combo-manual', 'onchange' => 'changeComboUser(' . $this->idGroup . ');'));

		$nicks = NotesPDF::getUsersinGroupWithHashtagAndGrade($this->idGroup, '%', '%');

		$this->out->element('option', array('value' => 'Todos'), _m('Todos'));

		for ($i = 0; $i < count($nicks); $i++) {
		    $this->out->element('option', array('value' => $nicks[$i]), $nicks[$i]);
		}

		$this->out->elementEnd('select');
		$this->out->elementEnd('div');
		$this->out->elementStart('div');
	
		$this->out->element('p', 'notes-manual-option', _m('Puntuación: '));
		$this->out->elementStart('select', array('name' => 'combo-grade', 'id' => 'notes-combo-grade', 'class' => 'notes-combo-manual', 'onchange' => 'changeComboGrade(' . $this->idGroup . ');'));
		$grades = NotesPDF::getGradesinGroupWithTagAndUser($this->idGroup, '%', '%');
		$this->out->element('option', array('value' => 'Todos'), _m('Todos'));
		for ($i = 0; $i < count($grades); $i++) {
		    $this->out->element('option', array('value' => $grades[$i]), $grades[$i]);
		}
		$this->out->elementEnd('select');
		$this->out->elementEnd('div');
	} 


        $this->out->submit('notes-submit-manual', _m('BUTTON', 'Aceptar'), 'submit', 'submit-custom');

        $this->out->elementEnd('div');
    }

}
