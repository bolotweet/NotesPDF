<?php

/**
 * 
 *  Bolotweet-Notes
    Copyright (C) 2018  bolotweet

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author   Alvaro Ortego <alvorteg@ucm.es>
 *
 */
if (!defined('STATUSNET') && !defined('LACONICA')) {
    exit(1);
}

class Notesgroupsform extends Form {

    protected $idGroup = null;
    protected $disabled = null;

    function __construct($out = null, $id, $disabled = 'false') {
        parent::__construct($out);

        $this->idGroup = $id;
        $this->disabled = $disabled;
    }

    function id() {

        return 'notes-groups' . $this->idGroup;
    }

    function action() {

        return common_local_url('notescustomize');
    }

    function sessionToken() {
        $this->out->hidden('token-' . $this->idGroup, common_session_token());
    }

    function formClass() {
        return 'form_notes_groups';
    }

    function formData() {


        $this->out->hidden('group-h-' . $this->idGroup, $this->idGroup, 'idGroup');

        if ($this->disabled == 'true') {
            $this->out->element('input', array('type' => 'submit',
                'id' => 'notes-submit-' . $this->idGroup,
                'class' => 'submit',
                'value' => _m('Generar Apuntes'),
                'title' => _m('Genera apuntes de este grupo en PDF'),
                'disabled' => $this->disabled));
        } else {

            $this->out->element('input', array('type' => 'submit',
                'id' => 'notes-submit-' . $this->idGroup,
                'class' => 'submit',
                'value' => _m('Generar Apuntes'),
                'title' => _m('Genera apuntes de este grupo en PDF')));
        }
    }

}
