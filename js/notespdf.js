/**
 * 
 *  Bolotweet-Notes
    Copyright (C) 2018  bolotweet

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author   Alvaro Ortego <alvorteg@ucm.es>
 *
 */



function changeComboTag(groupid) {

    var tag = $('#notes-combo-hashtag').find(":selected").val();
    var userid = $('#notes-combo-user').find(":selected").val();
    var grade = $('#notes-combo-grade').find(":selected").val();

    $('#notes-combo-user').load("../../local/plugins/NotesPDF/scripts/updateBoxUser.php", {groupid: groupid, grade: grade, tag: tag}, function() {

        if (userid !== 'Todos' ) {
            $("#notes-combo-user option[value=" + userid + "]").attr("selected", "selected");
        }

    });

    $('#notes-combo-grade').load("../../local/plugins/NotesPDF/scripts/updateBoxGrade.php", {groupid: groupid, userid: userid, tag: tag}, function() {

        if (grade !== 'Todos' ) {
            $("#notes-combo-grade option[value=" + grade + "]").attr("selected", "selected");
        }
    });


}

function changeComboUser(groupid) {

    var tag = $('#notes-combo-hashtag').find(":selected").val();
    var userid = $('#notes-combo-user').find(":selected").val();
    var grade = $('#notes-combo-grade').find(":selected").val();

    $('#notes-combo-hashtag').load("../../local/plugins/NotesPDF/scripts/updateBoxTags.php", {groupid: groupid, userid: userid, grade: grade}, function() {

        if (tag !== 'Todos' ) {
            $("#notes-combo-hashtag option[value=" + tag + "]").attr("selected", "selected");
        }
    });



    $('#notes-combo-grade').load("../../local/plugins/NotesPDF/scripts/updateBoxGrade.php", {groupid: groupid, userid: userid, tag: tag}, function() {

        if (grade !== 'Todos') {
            $("#notes-combo-grade option[value=" + grade + "]").attr("selected", "selected");
            //alert();
        }
    });


}

function changeComboGrade(groupid) {

    var tag = $('#notes-combo-hashtag').find(":selected").val();
    var userid = $('#notes-combo-user').find(":selected").val();
    var grade = $('#notes-combo-grade').find(":selected").val();

    $('#notes-combo-user').load("../../local/plugins/NotesPDF/scripts/updateBoxUser.php", {groupid: groupid, grade: grade, tag: tag}, function() {

        if (userid !== 'Todos' ) {
            $("#notes-combo-user option[value=" + userid + "]").attr("selected", "selected");
        }
    });


    $('#notes-combo-hashtag').load("../../local/plugins/NotesPDF/scripts/updateBoxTags.php", {groupid: groupid, userid: userid, grade: grade}, function() {

        if (tag !== 'Todos' ) {
            $("#notes-combo-hashtag option[value=" + tag + "]").attr("selected", "selected");
        }
    });


}





